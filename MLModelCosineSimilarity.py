# from operator import itemgetter
import os
from os import listdir
from os.path import isfile, join
from math import log, sqrt
from collections import defaultdict
import nltk
print("Training Started ... ")
# nltk.download('punkt')

inverted_index = defaultdict(list)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEMPLATES_DIR = os.path.join(BASE_DIR , "text-search-engine\corpus")
onlyfiles = [f for f in listdir(TEMPLATES_DIR) if isfile(join(TEMPLATES_DIR, f))]
nos_of_documents = len(onlyfiles)
vects_for_docs = []  # we will need nos of docs number of vectors, each vector is a dictionary
document_freq_vect = {}  # sort of equivalent to initializing the number of unique words to 0

class CosineVectorSimilarity(object):
    '''
    class docs
    '''

    def __init__(self):
        '''
        Constructor
        '''

    # this is the first function that is executed.
    def iterate_over_all_docs(self):
        for file in onlyfiles:
            token_list = self.get_tokenized_and_normalized_list(str(file))
            vect = self.create_vector(token_list)
            vects_for_docs.append(vect)


    # creates a vector from a query in the form of a list (l1) , vector is a dictionary, containing words:frequency pairs
    def create_vector_from_query(self , l1):
        vect = {}
        for token in l1:
            if token in vect:
                vect[token] += 1.0
            else:
                vect[token] = 1.0
        return vect


    # name is self explanatory, it generates and inverted index in the global variable inverted_index,
    # however, precondition is that vects_for_docs should be completely initialized
    def generate_inverted_index(self):
        count1 = 0
        for vector in vects_for_docs:
            print("#" , end="")
            for word1 in vector:
                inverted_index[word1].append(count1)
            count1 += 1


    # it updates the vects_for_docs global variable (the list of frequency vectors for all the documents)
    # and changes all the frequency vectors to tf-idf unit vectors (tf-idf score instead of frequency of the words)
    def create_tf_idf_vector(self):
        vect_length = 0.0
        for vect in vects_for_docs:
            print("#" , end="")
            for word1 in vect:
                word_freq = vect[word1]
                temp = self.calc_tf_idf(word1, word_freq)
                vect[word1] = temp
                vect_length += temp ** 2

            vect_length = sqrt(vect_length)
            for word1 in vect:
                vect[word1] /= vect_length


    # note: even though you do not need to convert the query vector into a unit vector,
    # I have done so because that would make all the dot products <= 1
    # as the name suggests, this function converts a given query vector
    # into a tf-idf unit vector(word:tf-idf vector given a word:frequency vector
    def get_tf_idf_from_query_vect(self , query_vector1):
        vect_length = 0.0
        for word1 in query_vector1:
            word_freq = query_vector1[word1]
            if word1 in document_freq_vect:  # I have left out any term which has not occurred in any document because
                query_vector1[word1] = self.calc_tf_idf(word1, word_freq)
            else:
                query_vector1[word1] = log(1 + word_freq) * log(
                    nos_of_documents)  # this additional line will ensure that if the 2 queries,
                # the first having all words in some documents,
                #   and the second having and extra word that is not in any document,
                # will not end up having the same dot product value for all documents
            vect_length += query_vector1[word1] ** 2
        vect_length = sqrt(vect_length)
        if vect_length != 0:
            for word1 in query_vector1:
                query_vector1[word1] /= vect_length


    # precondition: word is in the document_freq_vect
    # this function calculates the tf-idf score for a given word in a document
    def calc_tf_idf(self , word1, word_freq):
        return log(1 + word_freq) * log(nos_of_documents / document_freq_vect[word1])


    # this function returns the dot product of vector1 and vector2
    def get_dot_product(self , vector1, vector2):
        if len(vector1) > len(vector2):  # this will ensure that len(dict1) < len(dict2)
            temp = vector1
            vector1 = vector2
            vector2 = temp
        keys1 = vector1.keys()
        keys2 = vector2.keys()
        sum = 0
        for i in keys1:
            if i in keys2:
                sum += vector1[i] * vector2[i]
        return sum


    # this function returns a list of tokenized and stemmed words of any text
    def get_tokenized_and_normalized_list(self , doc_text):
        # return doc_text.split()

        tokens = nltk.word_tokenize(doc_text)
        ps = nltk.stem.PorterStemmer()
        stemmed = []
        for words in tokens:
            stemmed.append(ps.stem(words))
        return stemmed


    # creates a vector from a list (l1) , vector is a dictionary, containing words:frequency pairs
    # this function should not be called to parse the query given by the user
    # because this function also updates the document frequency dictionary
    def create_vector(self , l1):
        vect = {}  # this is a dictionary
        global document_freq_vect

        for token in l1:
            if token in vect:
                vect[token] += 1
            else:
                vect[token] = 1
                if token in document_freq_vect:
                    document_freq_vect[token] += 1
                else:
                    document_freq_vect[token] = 1
        return vect


    # this function takes the dot product of the query with all the documents
    #  and returns a sorted list of tuples of docId, cosine score pairs
    def get_result_from_query_vect(self , query_vector1):
        parsed_list = []
        for i in range(nos_of_documents - 1):
            dot_prod = self.get_dot_product(query_vector1, vects_for_docs[i])
            parsed_list.append((i, dot_prod))
            parsed_list = sorted(parsed_list, key=lambda x: x[1])
        return parsed_list

    print()
    def ask_query(self) : 
        while True:
            query = input("Please enter your query....")
            if len(query) == 0:
                break
            query_list = self.get_tokenized_and_normalized_list(query)
            query_vector = self.create_vector_from_query(query_list)
            self.get_tf_idf_from_query_vect(query_vector)
            result_set = self.get_result_from_query_vect(query_vector)
            print(result_set)

            for tup in result_set:
                print("The docid is " + str(tup[0]).zfill(4) + " and the weight is " + str(tup[1]))
            print()

class CosineSimilarity(object):
    '''
    class docs
    '''
    
    # this function takes the dot product of the query with all the documents
    #  and returns a sorted list of tuples of docId, cosine score pairs
    def simpleCosineSimilarity(self , X_list):
        list_of_docs = []
        l1 =[];l2 =[] 
        Y_list = onlyfiles
        X_set = set(list(X_list))
        for file in Y_list:
            Y_set = set(list(file))
            rvector = set()
            rvector = X_set.union(Y_set)
            l1 = [];l2 = []
            for w in rvector: 
                if w in X_set: l1.append(1) # create a vector 
                else: l1.append(0) 
                if w in Y_set: l2.append(1) 
                else: l2.append(0)
            c = 0
            # cosine formula  
            for i in range(len(rvector)): 
                    c+= l1[i]*l2[i] 
            cosine = c / float((sum(l1)*sum(l2))**0.5)
            threshold = 0.70
            if cosine > float(threshold) : 
                list_of_docs.append(file)
        return list_of_docs

if __name__ == '__main__':

    cvs = CosineVectorSimilarity()
    cs = CosineSimilarity()
    X_list = input("Enter the File : ")
    files = cs.simpleCosineSimilarity(X_list)
    print(files)
