from tkinter import Tk, Button,Label,Scrollbar,Listbox,StringVar,Entry,W,E,N,S,END
from tkinter import ttk
from tkinter import messagebox
import sqlite3
from sqlite3 import Error
from PIL import ImageTk,Image
from MLModelCosineSimilarity import CosineSimilarity, CosineVectorSimilarity
# from sqlserver_connfig import dbconnfig
# import pypyodbc as pyo
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.join(BASE_DIR , "text-search-engine")
print(BASE_DIR)
db_path = os.path.join(BASE_DIR, 'db.sqlite3')
conn = sqlite3.connect(db_path)


class Bookdb:
    '''
    class docs
    '''
    def __init__(self):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        db_path = os.path.join(BASE_DIR, 'db.sqlite3')
        self.conn = sqlite3.connect(db_path)
        # self.conn = pyo.connect(**dbconnfig)
        self.cursor = conn.cursor()
        print("You have connected to the  database")
        print(conn)

    def __del__(self):
        self.conn.close()

    def view(self):
        self.cursor.execute("SELECT * FROM documents")
        rows = self.cursor.fetchall()
        return rows

    def insert(self, Book_Name , Book_Url):
        sql=("INSERT INTO documents(Book_Name , Book_Url)VALUES (?,?)")
        values =[Book_Name , Book_Url]
        self.cursor.execute(sql,values)
        self.conn.commit()
        messagebox.showinfo(title="Book Database",message="New book added to database")

    def update(self, ID, Book_Name , Book_Url):
        tsql = 'UPDATE documents SET  Book_Name = ?, Book_Url = ? WHERE ID=?'
        self.cursor.execute(tsql, [Book_Name , Book_Url , ID])
        self.conn.commit()
        messagebox.showinfo(title="Book Database",message="Book Updated")

    def delete(self, ID):
        delquery ='DELETE FROM documents WHERE ID = ?'
        self.cursor.execute(delquery, [ID])
        self.conn.commit()
        messagebox.showinfo(title="Book Database",message="Book Deleted")

db = Bookdb()

def get_selected_row(event):
    global selected_tuple
    index = list_bx.curselection()[0]
    selected_tuple = list_bx.get(index)
    title_entry.delete(0, 'end')
    title_entry.insert('end', selected_tuple[1])
    author_entry.delete(0, 'end')
    author_entry.insert('end', selected_tuple[2])
    # isbn_entry.delete(0, 'end')
    # isbn_entry.insert('end', selected_tuple[3])

def view_records():
    list_bx.delete(0, 'end')
    for row in db.view():
        list_bx.insert('end', row)

def add_book():
    db.insert(Book_Name.get(), Book_Url.get())
    list_bx.delete(0, 'end')
    list_bx.insert('end', (Book_Name.get(), Book_Url.get()))
    title_entry.delete(0, "end") # Clears input after inserting
    author_entry.delete(0, "end")
    # isbn_entry.delete(0, "end")
    conn.commit()

def MLModel():
    cvs = CosineVectorSimilarity()
    cs = CosineSimilarity()
    X_list = str(search_entry.get())
    list_bx.delete(0, 'end')
    files = cs.simpleCosineSimilarity(X_list)
    if files is None:
        list_bx.insert('end', "No related documents Found in the DataBase")
    for file in files : 
        list_bx.insert('end', (file))
    search_entry.delete(0 , "end")

def delete_records():
    db.delete(selected_tuple[0])
    conn.commit()

def clear_screen():
    list_bx.delete(0,'end')
    title_entry.delete(0,'end')
    author_entry.delete(0,'end')
    # isbn_entry.delete(0,'end')

def update_records():
    # db.update(selected_tuple[0], title_text.get(), author_text.get(), isbn_text.get())
    title_entry.delete(0, "end") # Clears input after inserting
    author_entry.delete(0, "end")
    conn.commit()

def on_closing():
    dd = db
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()
        del dd


root = Tk()  # Creates application window

root.title("My documents Database Application") # Adds a title to application window
root.configure(background="gray11")  # Add background color to application window
root.geometry("850x500")  # Sets a size for application window
root.resizable(width=False,height=False) # Prevents the application window from resizing


# Create Labels and entry widgets

title_label =ttk.Label(root,text="Document Name",background="turquoise1",font=("TkDefaultFont", 16))
title_label.grid(row=2, column=0, padx = 10, sticky=W)
Book_Name = StringVar()
title_entry = ttk.Entry(root,width=24,textvariable=Book_Name)
title_entry.grid(row=2, column=1, sticky=W)

author_label =ttk.Label(root,text="Document URL",background="turquoise1",font=("TkDefaultFont", 16))
author_label.grid(row=2, column=2, padx = 20, sticky=W)
Book_Url = StringVar()
author_entry = ttk.Entry(root,width=24,textvariable=Book_Url)
author_entry.grid(row=2, column=3,  sticky=W)

search_label =ttk.Label(root,text="Search Document",background="turquoise1",font=("TkDefaultFont", 16))
search_label.grid(row=4, column=0, padx = 20, sticky=W)
Book_Url = StringVar()
search_entry = ttk.Entry(root,width=24,textvariable=Book_Url)
search_entry.grid(row=4, column=1,  sticky=W)


# Add a button to insert inputs into database

add_btn = Button(root, text="Add Document",bg="blue",fg="white",font="helvetica 10 bold",command=add_book)
add_btn.grid(row=0, column=4, sticky=W , pady = 20)

add_btn1 = Button(root, text="Search Document",bg="blue",fg="white",font="helvetica 10 bold",command=MLModel)
add_btn1.grid(row=4, column=2, sticky=W , padx = 10 , pady = 20)

# Add  a listbox  to display data from database
list_bx = Listbox(root,height=5,width=10,font="helvetica 13",bg="light blue")
list_bx.grid(row=5,column=0, columnspan=20,sticky=W + E,pady=150,padx=15)
list_bx.bind('<<ListboxSelect>>',get_selected_row)

# Add scrollbar to enable scrolling
scroll_bar = Scrollbar(root)
scroll_bar.grid(row=1,column=8, rowspan=14,sticky=W )

list_bx.configure(yscrollcommand=scroll_bar.set) # Enables vetical scrolling
scroll_bar.configure(command=list_bx.yview)

view_btn = Button(root, text="View all documents",bg="blue",fg="white",font="helvetica 10 bold",command=view_records)
view_btn.grid(row=3, column=4 , sticky=W , pady = 20)#, sticky=tk.N)

root.mainloop()  # Runs the application until exit
