# Text Based Search Engine #

## Installation process : ##

### 1.pip install -r requirement.txt ###

### 2.Machine Leaning Algorithms : ###

![Alt text](MlModels/MLModelsForString.PNG?raw=true)

### 3.Application GUI developed using Python Tkinter ###

![alt text](MlModels/GUI.PNG?raw=true)

### 4.Search Engine works ###

![alt text](MlModels/SearchEngine.PNG?raw=true)